## Seal Telecom

### Teste para vaga de desenvolvedor backend em Node JS
Este repositório foi criado com o objetivo de testar os candidatos para a vaga de desenvolvedor backend.
O teste é aplicado para candidatos de todos os níveis de experiência. Portanto, faça o teste tranquilamente independente do seu nível, entregando o que puder.

#### Instruções Básicas
1. Faça um fork deste repositório.
2. Faça a implementação do código seguindo as boas práticas de desenvolvimento.
3. Ideias para incrementar o teste serão bem vindas, sendo que os requisitos principais devem ser atingidos.
4. Após finalizar o teste, abra o arquivo README.md e escreva as intruções necessárias para instalar e executar sua entrega

#### Desafio: API Para cadastro e consulta de Equipamentos Técnicos

> O Seu desafio será desenvolver uma API Rest, usando Node JS para cadastro e consulta de *Equipamentos*, como Cabo de Rede, Roteadores e Switches, Monitores, Servidores, Câmeras, entre outros. Analise as User Stories e implemente-as conforme o foi identificado por você.

#####User Stories: 

1. O usuário deseja cadastrar um equipamento, e o mesmo deseja consultar os dados desse equipamento posteriormente (SKU, Descrição, Categoria, Preço Unitário)
2. O usuário deseja associar uma categoria a este equipamento técnico e também deseja editar essa categoria.
3. O usuário deseja consultar a lista de todos os equipamentos;
4. Também é de desejo do usuário filtar a lista de produtos por nome e categoria.
5. Caso o usuário identifique que o cadastro de um equipamento não esteja de acordo, o mesmo deseja editar esse item e atualizar esses dados.
6. Pode haver casos do usuário inativar o cadastro de um equipamento, por este ter saído do catálogo de itens.
7. Se o item foi cadastrado por engano, o usuário deseja excluir esse item.

#### O que esperamos no teste
* Um código organizado com nomes de variáveis e funções auto explicativas
* Uma boa estrutura de projeto
* Uma boa documentação para compilar o projeto corretamente
* Bom uso das rotas com base no padrão RESTFUL.
* Rotas entregando o HTTP status code correspondentes para cada tipo de operação.

#### Stack
* Node JS
* Express.JS
* MongoDB
* Docker será um bônus

#### Envio do teste
1. Suba o repositório no seu Gitlab e envie o link com o assunto: **Teste para Backend Dev Node JS** 
   para Renato Bezerra [rbezerra@sealtelecom.com.br](mailto:rbezerra@sealtelecom.com.br),
   com cópia para Renata Araujo [raraujo@sealtelecom.com.br](mailto:raraujo@sealtelecom.com.br)
